namespace DefaultNamespace
{
    public class IntModelAdapter: Model
    {
        private IntModel modelDelegate;
        private int _startValue;
        
        public IntModelAdapter(int startValue)
        {
            _startValue = startValue;
            modelDelegate = new IntModel(Model.ARR_LENGTH);
            modelDelegate.InitArr(_startValue);
        }

        public void SetArg()
        {
            int arg = 0;
            modelDelegate.SetArg(arg);
        }

        public void SetArgRef()
        {
            int arg = 1;
            modelDelegate.SetArgRef(ref _startValue);
        }

        public void SetArgOut()
        {
            int arg = 2;
            modelDelegate.SetArgOut(out _startValue);
        }

        public string GetDataAsString()
        {
            return modelDelegate.GetDataAsString();
        }
    }
}