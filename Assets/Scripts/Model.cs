namespace DefaultNamespace
{
    public interface Model
    {
        const int ARR_LENGTH = 10;
        void SetArg();
        void SetArgRef();
        void SetArgOut();
        string GetDataAsString();
    }
}