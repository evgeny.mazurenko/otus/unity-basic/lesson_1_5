using System;
using UnityEngine;

namespace DefaultNamespace
{
    public struct IntModel
    {
        private int[] _arr; 

        public IntModel(int arrLength)
        {
            _arr = new int[arrLength];
        }
        
        public void InitArr(int startValue)
        {
            _arr[0] = startValue;
            for (int i = 1; i < _arr.Length; i++)
            {
                checked
                {
                    try
                    {
                        _arr[i] = _arr[i - 1] * _arr[i - 1];
                    }
                    catch (OverflowException)
                    {
                        Debug.Log(
                            $"Integer Overflow on sqr of value {_arr[i - 1]}, setting at pos {i} Max Integer value {Int32.MaxValue}");
                        _arr[i] = Int32.MaxValue;
                    }
                }
            }
        }

        public void SetArg(int p)
        {
            Debug.Log($"Pass P value {p}");
        }

        public void SetArgRef(ref int p)
        {
            Debug.Log($"Pass P value {p} as ref");
        }

        public void SetArgOut(out int p)
        {
            p = Int32.MinValue;
            Debug.Log($"Return P value {p}");
        }

        public string GetDataAsString()
        {
            string result = "Type Struct: " + Int32.MaxValue.GetType() + "\r\n";
            result += "Arr Data: [";
            foreach (var item in _arr)
            {
                result += item + ";";
            }
            result = result.Substring(0, result.Length-1) + "]";
            return result;
        }
    }
}