using System.IO;
using DefaultNamespace;
using UnityEngine;

public class Main : MonoBehaviour
{
    private const string FILE_NAME = "./model_data.txt";
    private Model _model;
    
    // Start is called before the first frame update
    void Start()
    {
        CreateModel(true);
    }

    private void CallAllMethods()
    {
        CreateModel(true);
        SetArg();
        SetArgRef();
        SetArgRef();
        SetArgOut();
        WriteDataToFile();
    }
    
    public void SetIntModel(bool useInt)
    {
        CreateModel(useInt);
    }

    private void CreateModel(bool isIntModel)
    {
        if (isIntModel)
        {
            _model = new IntModelAdapter(2);
        }
        else
        {
            _model = new FloatModelAdapter(2.2f);
        }
    }

    public void SetArg()
    {
        _model.SetArg();
    }

    public void SetArgRef()
    {
        _model.SetArgRef();
    }

    public void SetArgOut()
    {
        _model.SetArgOut();
    }

    public void WriteDataToFile()
    {
        string modelData = _model.GetDataAsString();
        Debug.Log($"Writing Model data: {{{modelData}}}");
        using (StreamWriter sw = new StreamWriter(FILE_NAME, false))
        {
            sw.Write(modelData);
        }
    }


}
