using System;
using UnityEngine;

namespace DefaultNamespace
{
    public struct FloatModel
    {
        private float[] _arr;

        public FloatModel(int arrLength)
        {
            _arr = new float[arrLength];
        }

        public void InitArr(float startValue)
        {
            _arr[0] = startValue;
            for (int i = 1; i < _arr.Length; i++)
            {
                //Для типа с одинарной точностью ошибки переполнения не возникает, вместо этого присваетвается бесконесность.
                _arr[i] = _arr[i - 1] * _arr[i - 1];
                if (float.IsInfinity(_arr[i]))
                {
                    Debug.Log($"Infinity on sqr of value {_arr[i - 1]}, setting at pos {i} Max Float value {float.MaxValue}");
                    _arr[i] = float.MaxValue;
                }
            }
        }

        public void SetArg(float p)
        {
            Debug.Log($"Pass P value {p}");
        }

        public void SetArgRef(ref float p)
        {
            Debug.Log($"Pass P value {p} as ref");
        }

        public void SetArgOut(out float p)
        {
            p = float.MinValue;
            Debug.Log($"Return P value {p}");
        }

        public string GetDataAsString()
        {
            string result = "Type Struct: " + float.MaxValue.GetType() + "\r\n";
            result += "Arr Data: [";
            foreach (var item in _arr)
            {
                result += item + ";";
            }

            result = result.Substring(0, result.Length - 1) + "]";
            return result;
        }
    }
}