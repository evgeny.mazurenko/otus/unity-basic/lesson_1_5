namespace DefaultNamespace
{
    public class FloatModelAdapter: Model
    {
        private FloatModel modelDelegate;
        private float _startValue;
        
        public FloatModelAdapter(float startValue)
        {
            _startValue = startValue;
            modelDelegate = new FloatModel(Model.ARR_LENGTH);
            modelDelegate.InitArr(_startValue);
        }

        public void SetArg()
        {
            float arg = 0;
            modelDelegate.SetArg(arg);
        }

        public void SetArgRef()
        {
            float arg = 1;
            modelDelegate.SetArgRef(ref _startValue);
        }

        public void SetArgOut()
        {
            float arg = 2;
            modelDelegate.SetArgOut(out _startValue);
        }

        public string GetDataAsString()
        {
            return modelDelegate.GetDataAsString();
        }
    }
}